const gulp = require('gulp')
const accord = require('gulp-accord')
const header = require('gulp-header')

const git = require('git-state')
const gitpath = '.'

// Gather git info
let githeader = 'out-of-repository build'
if (git.isGitSync(gitpath)) {
    let check = git.checkSync(gitpath)
    let hash = git.commitSync(gitpath)
    let clean = check.dirty === 0 && check.untracked === 0

    githeader = `${check.branch}-${hash}`
    if (!clean) {
        githeader += ' [DIRTY]'
    }
}

gulp.task('style', function() {
    let date = new Date().toISOString()
    return gulp.src('./src/main.styl')
        .pipe(header(`/* reWIS ${githeader}, built on ${date} */`))
        .pipe(accord('stylus', {
            url: {
                name: 'inline-url',
                paths: [__dirname + '/img'],
                limit: false
            }
        }))
        .pipe(gulp.dest('./build'))
})

gulp.task('default', ['style'])
